﻿using UnityEngine;

namespace Assets.SceneEditor.Editor.LightsEditor
{
    public class LightsModel
    {
        public Light keyLight { get; set; }
        public GameObject keyLightGameObject { get; set; }
        public Light fillLight { get; set; }
        public GameObject fillLightGameObject { get; set; }
        public Light backLight { get; set; }
        public GameObject backLightGameObject { get; set; }

        public GameObject individualLightRigParent  {get; set; }
    }
}