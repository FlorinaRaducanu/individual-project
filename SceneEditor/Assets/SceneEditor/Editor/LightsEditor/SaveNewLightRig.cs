﻿using Assets.SceneEditor.Default_Light_Rigs;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Assets.SceneEditor.Game_Object_Manager;

namespace Assets.SceneEditor.Editor.LightsEditor
{
    public class SaveNewLightRig
    {
        //Default values for each set-up
        AngryLightRig angerLightRig = new AngryLightRig();
        HappyLightRig happyLightRig = new HappyLightRig();
        SadLightRig sadLightRig = new SadLightRig();
        DisgustLightRig disgustLightRig = new DisgustLightRig();
        SurprisedLightRig surprisedLightRig = new SurprisedLightRig();
        FearLightRig fearLightRig = new FearLightRig();

        //Values to be stored in a new light group 
        LightsModel lightsModel = new LightsModel();

        GameObjectManager gameObjectManager = new GameObjectManager();

        // This checks if there is any difference between the default values and the values to be saved in a new light group
        //by calling functions to compare positions, rotations, colours and intensities
        public bool CheckIfTheSettingsAreDifferent(string currentLightsGroupName, LightsModel newLights)
        {            
            bool isThereADifference = false;
            
            if (currentLightsGroupName == "Happy")
            {
                isThereADifference = CompareCurrentLightPositionWithNew(happyLightRig.lightRigTranslations, newLights);

                if (isThereADifference==true)
                {
                    return true;
                }
                else if (CompareCurrentColourWithNewLights(happyLightRig.lightRigColours, newLights) == true)
                {
                    return true;
                }
                else if (CompareCurrentIntensityWithNewLights(happyLightRig.lightsAdjustments, newLights) == true)
                {
                    return true;
                }

                
            }
            else if (currentLightsGroupName == "Disgust")
            {
                isThereADifference = CompareCurrentLightPositionWithNew(disgustLightRig.lightRigTranslations, newLights);

                if (isThereADifference == true)
                {
                    return true;
                }
                else if(CompareCurrentRotationsWithNewLights(disgustLightRig.lightRigTranslations, newLights))
                {
                    return true;
                }
                else if (CompareCurrentColourWithNewLights(disgustLightRig.lightRigColours, newLights) == true)
                {
                    return true;
                }
                else if (CompareCurrentIntensityWithNewLights(disgustLightRig.lightsAdjustments, newLights) == true)
                {
                    return true;
                }
            }
            else if (currentLightsGroupName == "Anger")
            {
                isThereADifference = CompareCurrentLightPositionWithNew(angerLightRig.lightRigTranslations, newLights);

                if (isThereADifference == true)
                {
                    return true;
                }
                else if (CompareCurrentColourWithNewLights(angerLightRig.lightRigColours, newLights) == true)
                {
                    return true;
                }
                else if (CompareCurrentIntensityWithNewLights(angerLightRig.lightsAdjustments, newLights) == true)
                {
                    return true;
                }
                
            }
            else if (currentLightsGroupName == "Fear")
            {
                isThereADifference = CompareCurrentLightPositionWithNew(fearLightRig.lightRigTranslations, newLights);

                if (isThereADifference == true)
                {
                    return true;
                }
                else if (CompareCurrentColourWithNewLights(fearLightRig.lightRigColours, newLights) == true)
                {
                    return true;
                }
                else if (CompareCurrentIntensityWithNewLights(fearLightRig.lightsAdjustments, newLights) == true)
                {
                    return true;
                }
                
            }
            else if (currentLightsGroupName == "Sad")
            {
                isThereADifference = CompareCurrentLightPositionWithNew(sadLightRig.lightRigTranslations, newLights);

                if (isThereADifference == true)
                {
                    return true;
                }
                else if (CompareCurrentColourWithNewLights(sadLightRig.lightRigColours, newLights) == true)
                {
                    return true;
                }
                else if (CompareCurrentIntensityWithNewLights(sadLightRig.lightsAdjustments, newLights) == true)
                {
                    return true;
                }
               
            }
            else if (currentLightsGroupName == "Surprise")
            {
                isThereADifference = CompareCurrentLightPositionWithNew(surprisedLightRig.lightRigTranslations, newLights);

                if (isThereADifference == true)
                {
                    return true;
                }
                else if (CompareCurrentColourWithNewLights(surprisedLightRig.lightRigColours, newLights) == true)
                {
                    return true;
                }
                else if (CompareCurrentIntensityWithNewLights(surprisedLightRig.lightsAdjustments, newLights) == true)
                {
                    return true;
                }
                
            }
            return isThereADifference;
        }

        public void SaveNewLightingSettings(string nameOfTheSetUp, GameObject newLights)
        {
            lightsModel.individualLightRigParent = new GameObject(nameOfTheSetUp);

            newLights.transform.Find("Key Light").transform.SetParent(lightsModel.individualLightRigParent.transform, false);
            newLights.transform.Find("Fill Light").transform.SetParent(lightsModel.individualLightRigParent.transform, false);
            newLights.transform.Find("Back Light").transform.SetParent(lightsModel.individualLightRigParent.transform, false);
        }

        public GameObject CreateNewLightingSetUpParent(string currentLightsGroupName, LightsModel newLights)
        {
            GameObject lightsModelGameObject = GameObject.Instantiate(newLights.keyLight.transform.parent.gameObject);
            GameObject clone = GameObject.Find(currentLightsGroupName + "(Clone)");
            gameObjectManager.DestroyGameObject(clone); ;

            return lightsModelGameObject;
        }

        bool CompareCurrentLightPositionWithNew(Dictionary<string, Vector3> defaultLights, LightsModel newLights)
        {
            bool isDifferentPosition = false;

            if (defaultLights["keyLightPosition"].x != newLights.keyLightGameObject.transform.position.x ||
                 defaultLights["keyLightPosition"].y != newLights.keyLightGameObject.transform.position.y ||
                 defaultLights["keyLightPosition"].z != newLights.keyLightGameObject.transform.position.z)
            {
                isDifferentPosition = true;
            }
            else if (defaultLights["fillLightPosition"].x != newLights.fillLightGameObject.transform.position.x ||
                     defaultLights["fillLightPosition"].y != newLights.fillLightGameObject.transform.position.y ||
                     defaultLights["fillLightPosition"].z != newLights.fillLightGameObject.transform.position.z)
            {
                isDifferentPosition = true;
            }
            else if (defaultLights["backLightPosition"].x != newLights.backLightGameObject.transform.position.x ||
                     defaultLights["backLightPosition"].y != newLights.backLightGameObject.transform.position.y ||
                     defaultLights["backLightPosition"].z != newLights.backLightGameObject.transform.position.z)
            {
                isDifferentPosition = true;

            }

            return isDifferentPosition;
        }

        bool CompareCurrentColourWithNewLights(Dictionary<string, Color> defaultLights, LightsModel newLights)
        {
            bool isDifferentColour = false;

            if(defaultLights["keyLight"] != newLights.keyLight.color ||
               defaultLights["fillLight"] != newLights.fillLight.color ||
               defaultLights["backLight"] != newLights.backLight.color)
            {
                isDifferentColour = true;
            }

            return isDifferentColour;
        }

        bool CompareCurrentIntensityWithNewLights(Dictionary<string, float> defaultLights, LightsModel newLights)
        {
            bool isDifferentColour = false;

            if (defaultLights["keyLightIntensity"] != newLights.keyLight.intensity ||
                defaultLights["fillLightIntensity"] != newLights.fillLight.intensity ||
                defaultLights["backLightIntensity"] != newLights.backLight.intensity)
            {            
                isDifferentColour = true;
            }

            return isDifferentColour;
        }

        bool CompareCurrentRotationsWithNewLights(Dictionary<string, Vector3> defaultLights, LightsModel newLights)
        {
            bool isDifferentColour = false;

            if (defaultLights["keyLightRotation"].x != newLights.keyLightGameObject.transform.rotation.x ||
                defaultLights["keyLightRotation"].y != newLights.keyLightGameObject.transform.rotation.y ||
                defaultLights["keyLightRotation"].z != newLights.keyLightGameObject.transform.rotation.z)
            {
                isDifferentColour = true;
            }
            else if (defaultLights["fillLightRotation"].x != newLights.fillLightGameObject.transform.rotation.x ||
                        defaultLights["fillLightRotation"].y != newLights.fillLightGameObject.transform.rotation.y ||
                        defaultLights["fillLightRotation"].z != newLights.fillLightGameObject.transform.rotation.z)
                
            {
                isDifferentColour = true;
            }
            else if (defaultLights["backLightRotation"].x != newLights.backLightGameObject.transform.rotation.x ||
                        defaultLights["backLightRotation"].y != newLights.backLightGameObject.transform.rotation.y ||
                        defaultLights["backLightRotation"].z != newLights.backLightGameObject.transform.rotation.z)

            {
                isDifferentColour = true;
            }

            return isDifferentColour;
        }

        bool CompareCurrentSpotAnglesWithNewLights(Dictionary<string, float> defaultLights, LightsModel newLights)
        {
            bool isDifferentColour = false;

            if (defaultLights["keyLightSpotAngle"] != newLights.keyLight.spotAngle ||
                defaultLights["fillLightSpotAngle"] != newLights.fillLight.spotAngle ||
                defaultLights["backLightSpotAngle"] != newLights.backLight.spotAngle)
            {
                isDifferentColour = true;
            }

            return isDifferentColour;
        }       
    }
}
