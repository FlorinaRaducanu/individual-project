﻿using System;
using System.Collections.Generic;
using Assets.SceneEditor.Default_Light_Rigs;
using Assets.SceneEditor.Game_Object_Manager;
using UnityEditor;
using UnityEngine;

namespace Assets.SceneEditor.Editor.LightsEditor
{
    public class LightsSettings
    {
        LightsFunctionsManager lightsFunctionsManager = new LightsFunctionsManager();
        GameObjectManager gameObjectManager = new GameObjectManager();
        HappyLightRig defaultHappySetUp = new HappyLightRig();
        SadLightRig defaultSadSetUp = new SadLightRig();
        AngryLightRig defaultAngerSetUp = new AngryLightRig();
        SurprisedLightRig defaultSurprisedSetUp = new SurprisedLightRig();
        FearLightRig defaultFearSetUp = new FearLightRig();
        DisgustLightRig defaultDisgustSetUp = new DisgustLightRig();

        Dictionary<string, Vector3> translations = new Dictionary<string, Vector3>();
        Dictionary<string, Color> colours = new Dictionary<string, Color>();
        Dictionary<string, float> intensities = new Dictionary<string, float>();

        Dictionary<string, float> isSelectedObjBiggerOrSmallerThanDefault = new Dictionary<string, float>();
        

        public LightsModel IsHappy(LightsModel lightsModel)
        {
            //Checks if light group exists in the scene by parent name 
            bool areLightsInTheScene = lightsFunctionsManager.IsLightAlreadyInTheScene(lightsModel);

            isSelectedObjBiggerOrSmallerThanDefault = gameObjectManager.CompareSelectedGameObjWithDefaultValues();

            lightsFunctionsManager.CreateThreePointsLightRig(lightsModel, "Happy");
            lightsFunctionsManager.SetLightsPositions(lightsModel, defaultHappySetUp.lightRigTranslations);
            lightsFunctionsManager.SetLightRigIntensities(lightsModel, defaultHappySetUp.lightsAdjustments);
            lightsFunctionsManager.SetLightsRanges(lightsModel, defaultHappySetUp.lightsAdjustments, isSelectedObjBiggerOrSmallerThanDefault);
            lightsFunctionsManager.SetLightTypes(lightsModel, LightType.Point, LightType.Point, LightType.Point);
            lightsFunctionsManager.SetLightRigColours(lightsModel, defaultHappySetUp.lightRigColours);

            //Enables lights if they don't exist in the scene already
            lightsFunctionsManager.EnableLightsIfTheyDontExistsAlready(lightsModel, areLightsInTheScene);

            return lightsModel;
        }

        

        public LightsModel IsSad(LightsModel lightsModel)
        {
            bool areLightsInTheScene;
            if (lightsModel.individualLightRigParent == null)
            {
                areLightsInTheScene = false;
            }
            else
            {
                areLightsInTheScene = true;
            }            

            isSelectedObjBiggerOrSmallerThanDefault = gameObjectManager.CompareSelectedGameObjWithDefaultValues();

            lightsFunctionsManager.CreateThreePointsLightRig(lightsModel, "Sad");
            lightsFunctionsManager.SetLightsPositions( lightsModel, defaultSadSetUp.lightRigTranslations);
            lightsFunctionsManager.SetLightRigIntensities(lightsModel, defaultSadSetUp.lightsAdjustments);
            lightsFunctionsManager.SetLightsRanges(lightsModel, defaultSadSetUp.lightsAdjustments, isSelectedObjBiggerOrSmallerThanDefault); 
            lightsFunctionsManager.SetLightTypes(lightsModel, LightType.Point, LightType.Point, LightType.Point);
            lightsFunctionsManager.SetLightRigColours(lightsModel, defaultSadSetUp.lightRigColours);
            lightsFunctionsManager.SetLightShadow(lightsModel.keyLight, LightShadows.Soft, 0.002f, 0.4f);

            if (areLightsInTheScene == false)
            {
                lightsModel.keyLight.enabled = true;
                lightsModel.fillLight.enabled = true;
                lightsModel.backLight.enabled = true;
            }
            else
            {
                lightsModel.keyLight.enabled = false;
                lightsModel.fillLight.enabled = false;
                lightsModel.backLight.enabled = false;
            }

            return lightsModel;
        }

        public LightsModel IsAnger(LightsModel lightsModel)
        {
            bool areLightsInTheScene;
            if (lightsModel.individualLightRigParent == null)
            {
                areLightsInTheScene = false;
            }
            else
            {
                areLightsInTheScene = true;
            }

            isSelectedObjBiggerOrSmallerThanDefault = gameObjectManager.CompareSelectedGameObjWithDefaultValues();

            lightsFunctionsManager.CreateThreePointsLightRig(lightsModel, "Anger"); 
            lightsFunctionsManager.SetLightsPositions(lightsModel, defaultAngerSetUp.lightRigTranslations);
            lightsFunctionsManager.SetLightRigRotation(lightsModel, defaultAngerSetUp.lightRigTranslations);
            lightsFunctionsManager.SetLightsRanges(lightsModel, defaultAngerSetUp.lightsAdjustments, isSelectedObjBiggerOrSmallerThanDefault);
            lightsFunctionsManager.SetSpotAngle(lightsModel, defaultAngerSetUp.lightsAdjustments);
            lightsFunctionsManager.SetLightRigIntensities(lightsModel, defaultAngerSetUp.lightsAdjustments);
            lightsFunctionsManager.SetLightTypes(lightsModel, LightType.Spot, LightType.Point, LightType.Point);
            lightsFunctionsManager.SetLightRigColours(lightsModel, defaultAngerSetUp.lightRigColours);
            lightsFunctionsManager.SetLightShadow(lightsModel.keyLight, LightShadows.Soft, 0f, 0.12f);

            if (areLightsInTheScene == false)
            {
                lightsModel.keyLight.enabled = true;
                lightsModel.fillLight.enabled = true;
                lightsModel.backLight.enabled = true;
            }
            else
            {
                lightsModel.keyLight.enabled = false;
                lightsModel.fillLight.enabled = false;
                lightsModel.backLight.enabled = false;
            }

            return lightsModel;
        }       

        public LightsModel IsFear(LightsModel lightsModel)
        {
            bool areLightsInTheScene = false;
            if (lightsModel.individualLightRigParent == null)
            {
                areLightsInTheScene = false;
            }
            else
            {
                areLightsInTheScene = true;
            }             

            isSelectedObjBiggerOrSmallerThanDefault = gameObjectManager.CompareSelectedGameObjWithDefaultValues();

            lightsFunctionsManager.CreateThreePointsLightRig(lightsModel, "Fear");
            lightsFunctionsManager.SetLightsPositions(lightsModel, defaultFearSetUp.lightRigTranslations);
            lightsFunctionsManager.SetLightRigRotation(lightsModel, defaultFearSetUp.lightRigTranslations);
            lightsFunctionsManager.SetLightsRanges(lightsModel, defaultFearSetUp.lightsAdjustments, isSelectedObjBiggerOrSmallerThanDefault);
            lightsFunctionsManager.SetSpotAngle(lightsModel, defaultFearSetUp.lightsAdjustments);
            lightsFunctionsManager.SetLightRigIntensities(lightsModel, defaultFearSetUp.lightsAdjustments);
            lightsFunctionsManager.SetLightTypes(lightsModel, LightType.Spot, LightType.Point, LightType.Point);                
            lightsFunctionsManager.SetLightRigColours(lightsModel, defaultFearSetUp.lightRigColours);
            lightsFunctionsManager.SetLightShadow(lightsModel.keyLight, LightShadows.Soft, 0.013f, 1.19f);

            if (areLightsInTheScene == false)
            {
                lightsModel.keyLight.enabled = true;
                lightsModel.fillLight.enabled = true;
                lightsModel.backLight.enabled = true;
            }
            else
            {
                lightsModel.keyLight.enabled = false;
                lightsModel.fillLight.enabled = false;
                lightsModel.backLight.enabled = false;
            }

            return lightsModel;
        }        

        public LightsModel IsSurprise(LightsModel lightsModel)
        {
            bool areLightsInTheScene = false;
            if (lightsModel.individualLightRigParent == null)
            {
                areLightsInTheScene = false;
            }
            else
            {
                areLightsInTheScene = true;
            }           

            isSelectedObjBiggerOrSmallerThanDefault = gameObjectManager.CompareSelectedGameObjWithDefaultValues();

            lightsFunctionsManager.CreateThreePointsLightRig(lightsModel, "Surprise");
            lightsFunctionsManager.SetLightsPositions(lightsModel, defaultSurprisedSetUp.lightRigTranslations);
            lightsFunctionsManager.SetLightRigRotation(lightsModel, defaultSurprisedSetUp.lightRigTranslations);
            lightsFunctionsManager.SetLightsRanges(lightsModel, defaultSurprisedSetUp.lightsAdjustments, isSelectedObjBiggerOrSmallerThanDefault);
            lightsFunctionsManager.SetSpotAngle(lightsModel, defaultSurprisedSetUp.lightsAdjustments);
            lightsFunctionsManager.SetLightRigIntensities(lightsModel, defaultSurprisedSetUp.lightsAdjustments);
            lightsFunctionsManager.SetLightTypes(lightsModel, LightType.Spot, LightType.Point, LightType.Point);
            lightsFunctionsManager.SetLightRigColours(lightsModel, defaultSurprisedSetUp.lightRigColours);
            lightsFunctionsManager.SetLightShadow(lightsModel.keyLight, LightShadows.Soft, 0.013f, 1.19f);

            if (areLightsInTheScene == false)
            {
                lightsModel.keyLight.enabled = true;
                lightsModel.fillLight.enabled = true;
                lightsModel.backLight.enabled = true;
            }
            else
            {
                lightsModel.keyLight.enabled = false;
                lightsModel.fillLight.enabled = false;
                lightsModel.backLight.enabled = false;
            }

            return lightsModel;
        }

        public LightsModel IsDisgust(LightsModel lightsModel)
        {
            isSelectedObjBiggerOrSmallerThanDefault = gameObjectManager.CompareSelectedGameObjWithDefaultValues();
            lightsFunctionsManager.CreateThreePointsLightRig(lightsModel, "Disgust");
            lightsFunctionsManager.SetLightsPositions(lightsModel, defaultDisgustSetUp.lightRigTranslations);
            lightsFunctionsManager.SetLightRigRotation(lightsModel, defaultDisgustSetUp.lightRigTranslations);
            lightsFunctionsManager.SetLightsRanges(lightsModel, defaultDisgustSetUp.lightsAdjustments, isSelectedObjBiggerOrSmallerThanDefault);
            lightsFunctionsManager.SetSpotAngle(lightsModel, defaultDisgustSetUp.lightsAdjustments);
            lightsFunctionsManager.SetLightRigIntensities(lightsModel, defaultDisgustSetUp.lightsAdjustments);
            lightsFunctionsManager.SetLightTypes(lightsModel, LightType.Spot, LightType.Point, LightType.Point);
            lightsFunctionsManager.SetLightRigColours(lightsModel, defaultDisgustSetUp.lightRigColours);
            lightsFunctionsManager.SetLightShadow(lightsModel.keyLight, LightShadows.Soft, 0.022f, 1.146f);            
            
            return lightsModel;
        }

        public void RemoveLightsFromTheScene()
        {
            gameObjectManager.DestroyGameObject(Selection.activeGameObject);
        }
    }
}