﻿using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;
using Assets.SceneEditor.Game_Object_Manager;
using Assets.SceneEditor.Default_Light_Rigs;


namespace Assets.SceneEditor.Editor.LightsEditor
{
    public class LightsFunctionsManager
    {
        public void SetSpotAngle(LightsModel lightModel, Dictionary<string, float> lightsSpotAngles)
        {
            lightModel.keyLight.spotAngle = lightsSpotAngles["keyLightSpotAngle"];
            lightModel.fillLight.spotAngle = lightsSpotAngles["fillLightSpotAngle"];
            lightModel.backLight.spotAngle = lightsSpotAngles["backLightSpotAngle"];
        }

        public void SetLightsPositions(LightsModel lightsModel, Dictionary<string, Vector3> defaultPositionSettings)
        {
            lightsModel.keyLightGameObject.transform.position = new Vector3(defaultPositionSettings["keyLightPosition"].x, defaultPositionSettings["keyLightPosition"].y, defaultPositionSettings["keyLightPosition"].z);
            lightsModel.fillLightGameObject.transform.position = new Vector3(defaultPositionSettings["fillLightPosition"].x, defaultPositionSettings["fillLightPosition"].y, defaultPositionSettings["fillLightPosition"].z);
            lightsModel.backLightGameObject.transform.position = new Vector3(defaultPositionSettings["backLightPosition"].x, defaultPositionSettings["backLightPosition"].y, defaultPositionSettings["backLightPosition"].z);
        }

        public void SetLightTypes(LightsModel lightModel, UnityEngine.LightType keyLightType,
                                    UnityEngine.LightType fillLightType, 
                                    UnityEngine.LightType backLightType)
        {
            lightModel.keyLight.type = keyLightType;
            lightModel.fillLight.type = fillLightType;
            lightModel.backLight.type = backLightType;
        }        

        public void SetLightsRanges(LightsModel lightsModel, Dictionary<string, float> defaultRangeSetUp, Dictionary<string, 
                                    float> isSelectedObjBiggerOrSmallerThanDefault)
        {

            float keyLightNewRange = 0.0f;
            float fillLightNewRange = 0.0f;
            float backLightNewRange = 0.0f;

            

            if (isSelectedObjBiggerOrSmallerThanDefault["width"] < isSelectedObjBiggerOrSmallerThanDefault["height"] &&
                     isSelectedObjBiggerOrSmallerThanDefault["width"] < isSelectedObjBiggerOrSmallerThanDefault["depth"])
            {
                keyLightNewRange = defaultRangeSetUp["keyLightRange"] * isSelectedObjBiggerOrSmallerThanDefault["width"];
                fillLightNewRange = defaultRangeSetUp["fillLightRange"] * isSelectedObjBiggerOrSmallerThanDefault["width"];
                backLightNewRange = defaultRangeSetUp["backLightRange"] * isSelectedObjBiggerOrSmallerThanDefault["width"];
            }
            else if (isSelectedObjBiggerOrSmallerThanDefault["height"] < isSelectedObjBiggerOrSmallerThanDefault["width"] &&
                     isSelectedObjBiggerOrSmallerThanDefault["height"] < isSelectedObjBiggerOrSmallerThanDefault["depth"])
            {
                keyLightNewRange = defaultRangeSetUp["keyLightRange"] * isSelectedObjBiggerOrSmallerThanDefault["height"];
                fillLightNewRange = defaultRangeSetUp["fillLightRange"] * isSelectedObjBiggerOrSmallerThanDefault["height"];
                backLightNewRange = defaultRangeSetUp["backLightRange"] * isSelectedObjBiggerOrSmallerThanDefault["height"];
            }
            else if (isSelectedObjBiggerOrSmallerThanDefault["depth"] < isSelectedObjBiggerOrSmallerThanDefault["width"] &&
                     isSelectedObjBiggerOrSmallerThanDefault["depth"] < isSelectedObjBiggerOrSmallerThanDefault["height"])
            {
                keyLightNewRange = defaultRangeSetUp["keyLightRange"] * isSelectedObjBiggerOrSmallerThanDefault["depth"];
                fillLightNewRange = defaultRangeSetUp["fillLightRange"] * isSelectedObjBiggerOrSmallerThanDefault["depth"];
                backLightNewRange = defaultRangeSetUp["backLightRange"] * isSelectedObjBiggerOrSmallerThanDefault["depth"];
            }
            else if (isSelectedObjBiggerOrSmallerThanDefault["depth"] == isSelectedObjBiggerOrSmallerThanDefault["width"] &&
                    isSelectedObjBiggerOrSmallerThanDefault["depth"] == isSelectedObjBiggerOrSmallerThanDefault["height"] &&
                    isSelectedObjBiggerOrSmallerThanDefault["width"] == isSelectedObjBiggerOrSmallerThanDefault["height"] &&
                    isSelectedObjBiggerOrSmallerThanDefault["depth"] > 1)
            {
                keyLightNewRange = defaultRangeSetUp["keyLightRange"] * isSelectedObjBiggerOrSmallerThanDefault["depth"];
                fillLightNewRange = defaultRangeSetUp["fillLightRange"] * isSelectedObjBiggerOrSmallerThanDefault["depth"];
                backLightNewRange = defaultRangeSetUp["backLightRange"] * isSelectedObjBiggerOrSmallerThanDefault["depth"];
            }
            else if (isSelectedObjBiggerOrSmallerThanDefault["depth"] >= 1 && 
                     isSelectedObjBiggerOrSmallerThanDefault["height"] >= 1 && 
                     isSelectedObjBiggerOrSmallerThanDefault["width"] >= 1 )
            {
                keyLightNewRange = defaultRangeSetUp["keyLightRange"];
                fillLightNewRange = defaultRangeSetUp["fillLightRange"];
                backLightNewRange = defaultRangeSetUp["backLightRange"];
            }

            lightsModel.keyLight.range = keyLightNewRange;
            lightsModel.fillLight.range = fillLightNewRange;
            lightsModel.backLight.range = backLightNewRange;
        }

        public void CreateThreePointsLightRig(LightsModel lightModel, string nameOfTheSetUp)
        {            
            lightModel.keyLightGameObject = new GameObject("Key Light");
            lightModel.fillLightGameObject = new GameObject("Fill Light");
            lightModel.backLightGameObject = new GameObject("Back Light");

            lightModel.keyLight = lightModel.keyLightGameObject.AddComponent<Light>();
            lightModel.fillLight = lightModel.fillLightGameObject.AddComponent<Light>();
            lightModel.backLight = lightModel.backLightGameObject.AddComponent<Light>();            

            lightModel.individualLightRigParent = new GameObject(nameOfTheSetUp);
            lightModel.keyLightGameObject.transform.SetParent(lightModel.individualLightRigParent.transform, false);
            lightModel.fillLightGameObject.transform.SetParent(lightModel.individualLightRigParent.transform, false);
            lightModel.backLightGameObject.transform.SetParent(lightModel.individualLightRigParent.transform, false);
        }       

        public void PositionLightGroupParent(LightsModel lightsModel, Vector3 parentPosition)
        {
            Transform lightGrouParent = lightsModel.keyLightGameObject.transform.parent;
            lightGrouParent.position = parentPosition;
        }

        public void SetLightRigIntensities(LightsModel lightModel, Dictionary<string, float> intensities)
        {
            lightModel.keyLight.intensity = intensities["keyLightIntensity"];
            lightModel.fillLight.intensity = intensities["fillLightIntensity"];
            lightModel.backLight.intensity = intensities["backLightIntensity"];
        }

        public void SetLightRigColours(LightsModel lightModel, Dictionary<string, Color> colours)
        {
            lightModel.keyLight.color = colours["keyLight"];
            lightModel.fillLight.color = colours["fillLight"];
            lightModel.backLight.color = colours["backLight"];            
        }

        public void SetLightRigRotation(LightsModel lightModel, Dictionary<string, Vector3> translations)
        {
            lightModel.keyLightGameObject.transform.rotation = Quaternion.Euler(translations["keyLightRotation"].x,
                                                                                            translations["keyLightRotation"].y,
                                                                                            translations["keyLightRotation"].z);

            lightModel.fillLightGameObject.transform.rotation = Quaternion.Euler(translations["fillLightRotation"].x,
                                                                                translations["fillLightRotation"].y,
                                                                                translations["fillLightRotation"].z);

            lightModel.backLightGameObject.transform.rotation = Quaternion.Euler(translations["backLightRotation"].x,
                                                                                translations["backLightRotation"].y,
                                                                                translations["backLightRotation"].z);
        }   

        public void SetLightShadow(Light light, LightShadows lightShadow, float shadowBias, float shadowNearPlane)
        {
            light.shadows = lightShadow;
            light.shadowBias = shadowBias;
            light.shadowNearPlane = shadowNearPlane;
        }

               

        public Dictionary<string, Vector3> GetLightsTranslations(Vector3 keyLightPosition, Vector3 fillLightPosition, Vector3 rimLightPosition)
        {
            Dictionary<string, Vector3> translations = new Dictionary<string, Vector3>();
            translations["keyLightPosition"] = keyLightPosition;

            translations["fillLightPosition"] = fillLightPosition;

            translations["backLightPosition"] = rimLightPosition;
            return translations;

        }

        public bool IsLightAlreadyInTheScene(LightsModel lightsModel)
        {
            bool areLightsInTheScene;
            if (lightsModel.individualLightRigParent == null)
            {
                areLightsInTheScene = false;
            }
            else
            {
                areLightsInTheScene = true;
            }

            return areLightsInTheScene;
        }

        public void EnableLightsIfTheyDontExistsAlready(LightsModel lightsModel, bool areLightsInTheScene)
        {
            if (areLightsInTheScene == false)
            {
                lightsModel.keyLight.enabled = true;
                lightsModel.fillLight.enabled = true;
                lightsModel.backLight.enabled = true;
            }
            else
            {
                lightsModel.keyLight.enabled = false;
                lightsModel.fillLight.enabled = false;
                lightsModel.backLight.enabled = false;
            }
        }

        

    }

}
