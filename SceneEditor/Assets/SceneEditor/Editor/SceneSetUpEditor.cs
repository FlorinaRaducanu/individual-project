﻿using System;
using UnityEditor;
using UnityEngine;
using Assets.SceneEditor.Input_From_CSV;
using Assets.SceneEditor.Editor.Camera_Control;
using Assets.SceneEditor.Game_Object_Manager;
using Assets.SceneEditor.Editor.LightsEditor;
using System.IO;

namespace Assets.SceneEditor.Editor
{
    public class SceneEditor : EditorWindow
    {
    
        //***Variables for Lights***

        private ReadEmotionFromCSV readEmotionFromCSV = new ReadEmotionFromCSV();
        //Holds the selected option 
        private string selectedSetup;        

        //Variables used for fold out light options e.g. if false, lights options are hidden
        bool isGroupKeyLight = true;
        bool isGroupFillLight = true;
        bool isGroupBackLight = true;        

        LightsSettings lightSettings = new LightsSettings();
        LightsModel lightsCreated = new LightsModel();
        SaveNewLightRig saveNewLightRig = new SaveNewLightRig();

        //***Variables for Camera ***

        CameraControl cameraControl = new CameraControl();    


        //***Recorder variables***

        string imageName = "Image Name";
        string statusOfRecorder = "Idle";
        string recordButton = "Record";
       

        //***UI variables***

        //Scroll bar position
        Vector2 scrollPosition = new Vector2();

        //Buttons 
        string newLightRigButton = "New Lighting Rig";

        //Tool bar with three buttons. 0 means that the Light button is selected by defaultlt 
        int toolbarMenuButtonSelected = 0;
        string[] toolbarMenuOptions = { "Light", "Camera", "Recorder" };

        //Array to display all the lighting setups available by options
        string[] lightingOptionsByEmotion =
        {
            "--Select setting--", "Happy", "Sad", "Surprise", "Fear", "Disgust", "Anger"            
        };

        //Index of the chosen lighting option
        int index = 0;        

        //Warning messages
        string selectGameObjectMessage = "***Please select one game objects***";
        string selectSetUptMessage = "***Please select one setup from the drop down list***";
        string deleteOnlySelectedObjectMessage = "Any selected object in the scene can be removed";

        GameObjectManager gameObjectManager = new GameObjectManager();
        LightsFunctionsManager lightsFunctionsManager = new LightsFunctionsManager();

        //Create window editor
        [MenuItem("Tools/SceneEditor")]
        static void Init()
        {
            EditorWindow window =
                (SceneEditor) EditorWindow.GetWindow(typeof(SceneEditor));
        }           

        void OnGUI()
        {
            try
            {
                //Initiates scroll bar
                scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, true, GUILayout.MaxHeight(1000), GUILayout.ExpandHeight(true));

                //Initiates toolbar menu with options from 0 to 2 where 0 is Lighting, 1 is Camera, and 2 is Recording 
                toolbarMenuButtonSelected = GUILayout.Toolbar(toolbarMenuButtonSelected, toolbarMenuOptions);

                //If Light Button is selected
                if (toolbarMenuButtonSelected == 0)
                {
                    //If the user choses to impot a csv to read the emotions, the index will be assigned the hiest value from the csv
                    ReadCSV();

                    //Drop down menu with the six emotions options
                    index = EditorGUILayout.Popup("Scene Set Up", index, lightingOptionsByEmotion);


                    if (Selection.activeGameObject)
                    {

                        //Generates a light rig based on the emotion selected by the user
                        GenerateLightingBasedOnEmotion();

                        //Dispalys the name of the selected set-up
                        EditorGUILayout.LabelField(selectedSetup);
                        EditorGUILayout.Space();

                        //The function call displays the UI elements in GUI
                        DisplayLightPositionInGui();

                        //Removes a selected object from the scene
                        RemoveObjectFromScene();

                        //If Save New Settings is clicked a new light group with the new settings will be saved in the hierarchy
                        SaveNewLightRigInHierarchy();

                    }
                    else
                    {
                        EditorGUILayout.HelpBox(selectGameObjectMessage, MessageType.Warning);
                    }
                }
                //If Camera Button is selected
                else if (toolbarMenuButtonSelected == 1)
                {
                    //Display gui elements
                    cameraControl.DisplayCameraGuiElements();

                    //Creates a checkbox that points the camera at the active object when it is ticked
                    cameraControl.SetCameraToLookAtTheSelectedObject();

                }
                //If Record Button is selected
                else if (toolbarMenuButtonSelected == 2)
                {
                    imageName = EditorGUILayout.TextField("File Name:", imageName);

                    if (GUILayout.Button(recordButton))
                    {
                        RecordImages();                         
                    }
                    EditorGUILayout.LabelField("Status: ", statusOfRecorder);
                }

                GUILayout.EndScrollView();
            }
            catch (NullReferenceException)
            {
                Debug.LogError("***The following error has occured in the SceneEditor***" + Environment.NewLine);

                //Catch error when the user tryes to save a light group without creating one first
                Debug.LogError("***Try to create light rig before trying to save another light group***");
            }
            catch (MissingReferenceException)
            {
                Debug.LogError("***The action can't be completed as there is a lighting set-up hasn't been generated yet*** ");
            }
            catch (ExitGUIException)
            {
                Debug.Log("***One of the colour fields was selected***");
            }
            catch (Exception e)
            {
                Debug.LogError("***The following error has occured in the SceneEditor: ***" + Environment.NewLine + e);
            }

        }

        private void GenerateLightingBasedOnEmotion()
        {
            //A light group will be created only if a game object will be selected
            if (index == 0)
            {
                EditorGUILayout.HelpBox(selectSetUptMessage, MessageType.Warning);
            }
            else if (GUILayout.Button(newLightRigButton) && index != 0)
            {
                //Call to process the selected emotion selected and generate the light rig
                selectedSetup = SelectSceneSetUp(index);
                //Position light rig at the position of the selected object
                lightsFunctionsManager.PositionLightGroupParent(lightsCreated, Selection.activeGameObject.transform.position);
            }
        }

        private void RemoveObjectFromScene()
        {
            EditorGUILayout.HelpBox(deleteOnlySelectedObjectMessage, MessageType.Info);
            if (GUILayout.Button("Remove from Scene"))
            {
                if (EditorUtility.DisplayDialog("Remove Selection from Scene",
                                                "Are you sure you want to remove " + Selection.activeGameObject.name + " from the scene?", "Yes", "No"))
                {
                    //Removes a selected game object                            
                    lightSettings.RemoveLightsFromTheScene();
                }
            }
        }

        void RecordImages()
        {
            statusOfRecorder = "Captured frame " + imageName;
            ScreenCapture.CaptureScreenshot(imageName + ".png");
        }


        public void ReadCSV()
        {
            int emotionDetected = 0;
            
            const string selectEmotionFromFile = "Choose File";
            string path = "";

            const int MAX_EMOTION_DETECTED = 6;

            string[] dataReadFromCsv = new string[MAX_EMOTION_DETECTED];

            if (GUILayout.Button(selectEmotionFromFile))
            {
                path = EditorUtility.OpenFilePanel("Load emotion", "", "csv");
                if (path.Length != 0)
                {
                    dataReadFromCsv = readEmotionFromCSV.ProcessCSV(path);
                    emotionDetected = readEmotionFromCSV.SelectEmotionFromCsv(dataReadFromCsv);
                    index = emotionDetected;
                }
            }            
        }

        private void SaveNewLightRigInHierarchy()
        {
            bool isThereADifferece = false;
            
            if (GUILayout.Button("Save New Settings") && index != 0)
            {
                string currentLightRig = "";
                string newLightGroupName = "New Light Rig";

                currentLightRig = lightsCreated.keyLight.transform.parent.name;   

                isThereADifferece = saveNewLightRig.CheckIfTheSettingsAreDifferent(currentLightRig, lightsCreated);
                if (isThereADifferece)
                {
                    GameObject newLightGroupParent = saveNewLightRig.CreateNewLightingSetUpParent(currentLightRig, lightsCreated);
                    saveNewLightRig.SaveNewLightingSettings(newLightGroupName, newLightGroupParent);
                }
                else if (!isThereADifferece)
                {
                    EditorGUILayout.HelpBox("change variables", MessageType.Warning);
                }

            } 

        }        

        string SelectSceneSetUp(int lightRigChosen)
        {            
            string selectedSetUp = null;
            if (lightRigChosen == 0)
            {
                EditorGUILayout.HelpBox(selectSetUptMessage, MessageType.Warning);
                lightsCreated = new LightsModel();
            }
            else if (lightRigChosen == 1)
            {
                lightsCreated = lightSettings.IsHappy(lightsCreated);
                selectedSetUp = "Happy Set-up";
            }
            else if (lightRigChosen == 2)
            {
                lightsCreated = lightSettings.IsSad(lightsCreated);
                selectedSetUp = "Sad Set-up";
            }
            else if (lightRigChosen == 3)
            {
                lightsCreated = lightSettings.IsSurprise(lightsCreated);
                selectedSetUp = "Surprise Set-up";
            }
            else if (lightRigChosen == 4)
            {
                lightsCreated = lightSettings.IsFear(lightsCreated);
                selectedSetUp = "Fear Set-up";
            }
            else if (lightRigChosen == 5)
            {
                lightsCreated = lightSettings.IsDisgust(lightsCreated);
                selectedSetUp = "Disgust Set-up";
            }          
            else if (lightRigChosen == 6)
            {
                lightsCreated = lightSettings.IsAnger(lightsCreated);
                selectedSetUp = "Anger Set-up";
            }

            return selectedSetUp;
        }
       

        private void DisplayLightPositionInGui()
        {
            isGroupKeyLight = EditorGUILayout.BeginFoldoutHeaderGroup(isGroupKeyLight, "Key Light");            
            if (isGroupKeyLight && lightsCreated.keyLightGameObject != null)
            {
                //Key Light Position
                lightsCreated.keyLightGameObject.transform.position = EditorGUILayout.Vector3Field("Key Light Position",
                                                                                                    lightsCreated.keyLightGameObject.transform.position);

                //Key Light Rotation
                Vector4 keyLightRotationVector4 = EditorGUILayout.Vector4Field("Key Light Rotation:", gameObjectManager.QuaternionToVector4(lightsCreated.keyLightGameObject.transform.localRotation));
                lightsCreated.keyLightGameObject.transform.localRotation = gameObjectManager.ConvertToQuaternion(keyLightRotationVector4);


                //Key Light Intensity
                lightsCreated.keyLight.intensity = EditorGUILayout.Slider("Key Light Intensity", lightsCreated.keyLight.intensity, 0, 10);


                //Key Light Range
                lightsCreated.keyLight.range = EditorGUILayout.Slider("Key Light Range", lightsCreated.keyLight.range, 0, 100);


                //Key Light Colour
                lightsCreated.keyLight.color = EditorGUILayout.ColorField("Key Light Colour", lightsCreated.keyLight.color);
                EditorGUILayout.Space();
                EditorGUILayout.Space();

                //status = lightsCreated.keyLightGameObject.tag;
            }

            EditorGUILayout.EndFoldoutHeaderGroup();

            isGroupFillLight = EditorGUILayout.BeginFoldoutHeaderGroup(isGroupFillLight, "Fill Light");
            if (isGroupFillLight && lightsCreated.fillLightGameObject != null)
            {
                lightsCreated.fillLight.type = UnityEngine.LightType.Point;
                
                //Fill Light Position
                lightsCreated.fillLightGameObject.transform.position =
                    EditorGUILayout.Vector3Field("Fill Light Position", lightsCreated.fillLightGameObject.transform.position);
                

                //Fill Light Rotation
                Vector4 fillLightRotationVector4 = EditorGUILayout.Vector4Field("Fill Light Rotation:", gameObjectManager.QuaternionToVector4(lightsCreated.fillLightGameObject.transform.localRotation));
                lightsCreated.fillLightGameObject.transform.localRotation = gameObjectManager.ConvertToQuaternion(fillLightRotationVector4);
                
                
                //Fill Light Intensity
                lightsCreated.fillLight.intensity = EditorGUILayout.Slider("Fill Light Intensity", lightsCreated.fillLight.intensity, 0, 10);

                //Key Light Range
                lightsCreated.fillLight.range = EditorGUILayout.Slider("Fill Light Range", lightsCreated.fillLight.range, 0, 100);

                //Fill Light Colour
                lightsCreated.fillLight.color = EditorGUILayout.ColorField("Fill Light Colour", lightsCreated.fillLight.color);

                EditorGUILayout.Space();
                EditorGUILayout.Space();

                //status = lightsCreated.fillLightGameObject.tag;
            }
            EditorGUILayout.EndFoldoutHeaderGroup();

            isGroupBackLight = EditorGUILayout.BeginFoldoutHeaderGroup(isGroupBackLight, "Backlights");
            if (isGroupBackLight && lightsCreated.backLightGameObject != null)
            {
                lightsCreated.backLight.type = UnityEngine.LightType.Point;

                //Back Light Position
                lightsCreated.backLightGameObject.transform.position =
                    EditorGUILayout.Vector3Field("Back Light Position", lightsCreated.backLightGameObject.transform.position);

                //Back Light Rotation
                Vector4 backLightRotationVector4 = EditorGUILayout.Vector4Field("Back Light Rotation:",
                                                      gameObjectManager.QuaternionToVector4(lightsCreated.backLightGameObject.transform.localRotation));
                lightsCreated.backLightGameObject.transform.localRotation = gameObjectManager.ConvertToQuaternion(backLightRotationVector4);
                

                //Back Light Intensity
                lightsCreated.backLight.intensity =
                    EditorGUILayout.Slider("Back Light Intensity", lightsCreated.backLight.intensity, 0, 10);

                //Back Light Range
                lightsCreated.backLight.range = EditorGUILayout.Slider("Back Light Range", lightsCreated.backLight.range, 0, 100);

                //Back Light Colour
                lightsCreated.backLight.color =
                    EditorGUILayout.ColorField("Back Light Colour", lightsCreated.backLight.color);
                EditorGUILayout.Space();
                EditorGUILayout.Space();
            }
            EditorGUILayout.EndFoldoutHeaderGroup();
        }

        


    }
}