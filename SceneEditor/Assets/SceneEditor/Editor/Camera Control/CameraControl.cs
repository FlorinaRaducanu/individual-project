﻿using UnityEditor;
using UnityEngine;
using Assets.SceneEditor.Game_Object_Manager;
namespace Assets.SceneEditor.Editor.Camera_Control
{
    public class CameraControl
    {
        //Warnign message
        string selectGameObjectMessage = "***Please select one game objects***";

        //Value to initiate camera LookAt checkbox. In this case LookAt function will be disabled until the user enables it
        bool lookAtCharacterButton = false;

        //Variables used for fold out camera options
        bool isCamera = true;

        GameObjectManager gameObjectManager = new GameObjectManager();

        public void SetCameraToLookAtTheSelectedObject()
        {
            GUI.enabled = Selection.activeGameObject;
            lookAtCharacterButton = EditorGUILayout.Toggle("Look at selected object", lookAtCharacterButton);
            if (GUI.enabled && Selection.activeGameObject)
            {
                if (lookAtCharacterButton)
                {
                    GUI.enabled = Selection.activeGameObject;
                    Camera.main.transform.LookAt(Selection.activeTransform.position);
                }
            }
            else if (!Selection.activeGameObject)
            {
                GUI.enabled = false;
                EditorGUILayout.HelpBox(selectGameObjectMessage, MessageType.Warning);
            }
        }


        public void DisplayCameraGuiElements()
        {
            //Initialises fold out menu fro camera
            isCamera = EditorGUILayout.BeginFoldoutHeaderGroup(isCamera, "Camera");
            if (isCamera)
            {
                //Camera Position
                Camera.main.transform.position = EditorGUILayout.Vector3Field("Camera Position:", Camera.main.transform.position);
                EditorGUILayout.Space();

                //Camera Rotation
                Vector4 cameraRotationVector4 = EditorGUILayout.Vector4Field("Camera Rotation:", gameObjectManager.QuaternionToVector4(Camera.main.transform.localRotation));
                Camera.main.transform.localRotation = gameObjectManager.ConvertToQuaternion(cameraRotationVector4);
                EditorGUILayout.Space();
            }
            EditorGUILayout.EndFoldoutHeaderGroup();
        }
    }
}
