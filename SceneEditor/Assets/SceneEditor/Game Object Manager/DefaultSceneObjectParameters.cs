﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Rendering;

namespace Assets.SceneEditor.Game_Object_Manager
{
    public class DefaultSceneObjectParameters
    {
        public float defaultWidth = 1.06f;
        public float defaultHeight = 1.98f;
        public float defaultDepth = 0.82f;
    }
}
