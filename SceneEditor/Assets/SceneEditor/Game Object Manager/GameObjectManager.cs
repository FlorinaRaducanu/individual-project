﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.SceneEditor.Game_Object_Manager
{
    public class GameObjectManager
    {
        DefaultSceneObjectParameters defaultSceneObjectParameters = new DefaultSceneObjectParameters();
        public void DestroyGameObject(GameObject subjectGameObject)
        {
            /**
             * https://forum.unity.com/threads/onvalidate-and-destroying-objects.258782/
             */
            UnityEditor.EditorApplication.delayCall += () =>
            {
                MonoBehaviour.DestroyImmediate(subjectGameObject);
            };
        }

        /**
         * Author:robertbu
         * Date:04/08/2013
         * Title of source code:N/A
         * Code version:1
         * Type: source code
         * Web Address: https://answers.unity.com/questions/507556/how-to-calculate-the-total-size-of-objects-on-the.html
         */
        public Dictionary<string, float> CheckTotalSize(GameObject subject)
        {
            Bounds subjectBounds;
            Dictionary<string, float> sizeOfGameObject = new Dictionary<string, float>()
            {
                {"totalWidth", 0},
                {"totalHeight", 0},
                {"totalDepth", 0}
            };

            subjectBounds = getBounds(subject);
            sizeOfGameObject["totalWidth"] = sizeOfGameObject["totalWidth"] + subjectBounds.size.x;
            sizeOfGameObject["totalHeight"] = sizeOfGameObject["totalHeight"] + subjectBounds.size.y;
            sizeOfGameObject["totalDepth"] = sizeOfGameObject["totalDepth"] + subjectBounds.size.z;

    
            return sizeOfGameObject;
        }

        Bounds getBounds(GameObject subject)
        {
            Bounds bounds;
            Renderer childRender;
            bounds = getRenderBounds(subject);
            if (bounds.extents.x == 0)
            {
                bounds = new Bounds(subject.transform.position, Vector3.zero);
                foreach (Transform child in subject.transform)
                {
                    childRender = child.GetComponent<Renderer>();
                    if (childRender)
                    {
                        bounds.Encapsulate(childRender.bounds);
                    }
                    else
                    {
                        bounds.Encapsulate(getBounds(child.gameObject));
                    }
                }
            }
            return bounds;
        }

        Bounds getRenderBounds(GameObject subject)
        {
            Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
            Renderer render = subject.GetComponent<Renderer>();
            if (render != null)
            {
                return render.bounds;
            }
            return bounds;
        }

        public Dictionary<string, float> CompareSelectedGameObjWithDefaultValues()
        {
            Dictionary<string, float> activeObjectSize = CheckTotalSize(Selection.activeGameObject);
            //This dictionary will store how bigger or 
            //smaller the selected game object is in comparison with the default sizes
            Dictionary<string, float> resultsOfComparedValues = new Dictionary<string, float>();

            float resultForWidth = activeObjectSize["totalWidth"] / defaultSceneObjectParameters.defaultWidth;
            resultsOfComparedValues.Add("width", resultForWidth);

            float resultForHeight = activeObjectSize["totalHeight"] / defaultSceneObjectParameters.defaultHeight;
            resultsOfComparedValues.Add("height", resultForHeight);

            float resultForDepth = activeObjectSize["totalHeight"] / defaultSceneObjectParameters.defaultDepth;
            resultsOfComparedValues.Add("depth", resultForDepth);

            return resultsOfComparedValues;
        }

        public Quaternion ConvertToQuaternion(Vector4 v4)
        {
            return new Quaternion(v4.x, v4.y, v4.z, v4.w);
        }


        public Vector4 QuaternionToVector4(Quaternion rot)
        {
            return new Vector4(rot.x, rot.y, rot.z, rot.w);
        }
    }
}
