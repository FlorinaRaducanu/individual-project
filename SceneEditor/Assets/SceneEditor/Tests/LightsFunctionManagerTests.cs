﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class LightsFunctionManagerTests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void LightsFunctionManagerTestsSimplePasses()
        {
            // Use the Assert class to test conditions
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        //[UnityTest]
        //public IEnumerator lightsFunctionManagerTestsWithEnumeratorPasses()
        //{
            //float mockNewKeyLightSpotAngle = 0.0f;
            //float mockNewFillLightSpotAngle = 0.0f;
            //float mockNewBackLightSpotAngle = 0.0f;

            //LightsModel newLightsModel = new LightsModel();

            //float mockDefaultKeyLightSpotAngle = 2.0f;
            //float mockDefaultFillLightSpotAngle = 3.0f;
            //float mockDefaultBackLightSpotAngle = 4.0f;

            //Assert.AreEqual(mockNewKeyLightSpotAngle, mockDefaultKeyLightSpotAngle);
            //Assert.AreEqual(mockNewFillLightSpotAngle, mockDefaultFillLightSpotAngle);
            //Assert.AreEqual(mockNewBackLightSpotAngle, mockDefaultBackLightSpotAngle);
        //}
    }
}
