﻿using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Assets.SceneEditor.Input_From_CSV
{
    public class ReadEmotionFromCSV
    {
        public int ReadCSV()
        {
            int emotionDetected = 0;
            const string selectEmotionFromFile = "Select Emotion";
            string path = "";
            int indexForEmotionSelected = 0;
            const int MAX_EMOTION_DETECTED = 6;

            string[] dataReadFromCsv = new string[MAX_EMOTION_DETECTED];
            if (GUILayout.Button(selectEmotionFromFile))
            {
                path = EditorUtility.OpenFilePanel("Load emotion", "", "csv");
                if (path.Length != 0)
                {
                    dataReadFromCsv = ProcessCSV(path);
                    emotionDetected = SelectEmotionFromCsv(dataReadFromCsv);
                    indexForEmotionSelected = emotionDetected;
                }
            }

            return indexForEmotionSelected;
        }

        /**
         * The function loads 
        **/
        public int SelectEmotionFromCsv(string[] dataReadFromCSV)
        {                   

            DetectedEmotion emotion = new DetectedEmotion();
            float maxValue;
            
            maxValue = 0.0f;
            int index = 0;
            string[] row = dataReadFromCSV[1].Split(new char[] { ',' });

            float.TryParse(row[3], out emotion.happy);
            if (emotion.happy > maxValue)
            {
                maxValue = emotion.happy;
                index = 1;
            }

            float.TryParse(row[4], out emotion.sad);
            if (emotion.sad > maxValue)
            {
                maxValue = emotion.sad;
                index = 2;
            }

            float.TryParse(row[5], out emotion.surprise );
            if (emotion.surprise > maxValue)
            {
                maxValue = emotion.surprise;
                index = 3;
            }

            float.TryParse(row[6], out emotion.fear );
            if (emotion.fear > maxValue)
            {
                maxValue = emotion.fear;
                index = 4;
            }

            float.TryParse(row[7], out emotion.disgust);
            if (emotion.disgust > maxValue)
            {
                maxValue = emotion.disgust;
                index = 5;
            }

            float.TryParse(row[8], out emotion.anger);
            if (emotion.anger > maxValue)
            {
                index = 6;
            }   
            

            return index;
        }

        public string[] ProcessCSV(string path)
        {
            string[] dataReadFromCSV;
            string fileContent = File.ReadAllText(path);
            TextAsset emotionsDetected = new TextAsset(fileContent);
            dataReadFromCSV = emotionsDetected.text.Split(new char[] { '\n' });

            return dataReadFromCSV;
        }

        //https://stackoverflow.com/questions/3464934/get-max-value-from-listmytype
        public string FindEmotionDetectedByMaxValueInEmotionsList(Dictionary<string, float> detectedEmotion)
        {
            if(detectedEmotion.Count == 0)
            {
                throw new InvalidOperationException("Empty list");
            }

            float maxValue = float.MinValue;
            string emotionFound = "";
            foreach (KeyValuePair<string, float> em in detectedEmotion)
            {                        
                    if (em.Value > maxValue)
                    {
                        emotionFound = em.Key;
                    }                            
            }

            return emotionFound;
        }      

    }
}
