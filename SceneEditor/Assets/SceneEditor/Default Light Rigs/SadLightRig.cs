﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.SceneEditor.Default_Light_Rigs
{
    public class SadLightRig
    {
        public Dictionary<string, Vector3> lightRigTranslations = new Dictionary<string, Vector3>()
            {
                {"keyLightPosition",  new Vector3(0.538f, 1.716f, 0.109f)},
                {"fillLightPosition",  new Vector3(-0.421f, 1.776f, 0.499f)},
                {"backLightPosition",  new Vector3(-0.58f, 1.869f, -0.07f)}
            };

        public Dictionary<string, Color> lightRigColours = new Dictionary<string, Color>()
            {
                {"keyLight", Color.white},
                {"fillLight", Color.white},
                {"backLight", new Color(57f, 95f, 224f, 255f)}
            };

        public Dictionary<string, float> lightsAdjustments = new Dictionary<string, float>()
            {
                {"keyLightIntensity", 1.3f},
                {"fillLightIntensity", 0.6f},
                {"backLightIntensity", 0.011f},

                {"keyLightRange", 2.42f},
                { "fillLightRange", 0.4f},
                { "backLightRange", 2.55f},
            };
    }
}
