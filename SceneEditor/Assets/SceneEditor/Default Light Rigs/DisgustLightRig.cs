﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.SceneEditor.Default_Light_Rigs
{
    public class DisgustLightRig
    {
        public Dictionary<string, Vector3> lightRigTranslations = new Dictionary<string, Vector3>()
        {
            {"keyLightPosition",  new Vector3(0.159f, 2.934f, 1.45f)},
            {"fillLightPosition",  new Vector3(0.048f, 1.651f, 0.359f)},
            {"backLightPosition",  new Vector3(-0.192f, 1.522f, -0.119f)},

            {"keyLightRotation", new Vector3(127.7f, 1.7f, 0.0f)},
            {"fillLightRotation", new Vector3(0, 0, 0)},
            {"backLightRotation", new Vector3(0, 0, 0)}
        };

        public Dictionary<string, Color> lightRigColours = new Dictionary<string, Color>()
        {
            {"keyLight", Color.white},
            {"fillLight", Color.white},
            {"backLight", new Color(72f, 199f, 85f, 255f)}
        };

        public Dictionary<string, float> lightsAdjustments = new Dictionary<string, float>()
        {
            {"keyLightIntensity", 1.2f},
            {"fillLightIntensity", 0.6f},
            {"backLightIntensity", 0.04f},

            {"keyLightRange", 7.1f},
            {"fillLightRange", 0.63f},
            {"backLightRange", 0.43f},

            {"keyLightSpotAngle", 29.2f},
            {"fillLightSpotAngle", 0f},
            {"backLightSpotAngle", 0f }

        };
    }
}
