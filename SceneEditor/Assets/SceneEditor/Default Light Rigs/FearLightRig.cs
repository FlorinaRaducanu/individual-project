﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.SceneEditor.Default_Light_Rigs
{
    public class FearLightRig
    {
        public Dictionary<string, Vector3> lightRigTranslations = new Dictionary<string, Vector3>()
        {
            {"keyLightPosition",  new Vector3(0.699f, 2.421f, 0.565f)},
            {"fillLightPosition",  new Vector3(-0.017f, 1.575f, 0.3741f)},
            {"backLightPosition",  new Vector3(-0.414f, 1.623f, -0.296f)},

            {"keyLightRotation", new Vector3(45f, -135f, 29.4f)},
            {"fillLightRotation", new Vector3(-74.2f, 214.67f, 0)},
            {"backLightRotation", new Vector3(5.05f, 138.4f, 0)}
        };

        public Dictionary<string, Color> lightRigColours = new Dictionary<string, Color>()
        {
            {"keyLight", Color.white},
            {"fillLight", Color.white},
            {"backLight", new Color(46f, 161f, 229f, 255f)}
        };

        public Dictionary<string, float> lightsAdjustments = new Dictionary<string, float>()
        {
            {"keyLightIntensity", 1.1f},
            {"fillLightIntensity", 0.3f},
            {"backLightIntensity", 0.011f},

            {"keyLightRange", 7.1f},
            {"fillLightRange", 0.34f},
            {"backLightRange", 2.58f},

            {"keyLightSpotAngle", 15f},
            {"fillLightSpotAngle", 0f},
            {"backLightSpotAngle", 0f }

        };
    }
}
