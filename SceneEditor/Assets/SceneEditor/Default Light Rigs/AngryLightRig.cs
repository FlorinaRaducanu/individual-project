﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.SceneEditor.Default_Light_Rigs
{
    public class AngryLightRig
    {
        public Dictionary<string, Vector3> lightRigTranslations = new Dictionary<string, Vector3>()
        {
            {"keyLightPosition",  new Vector3(0.829f, 2.695f, 0.639f)},
            {"fillLightPosition",  new Vector3(-0.146f, 1.559f, 0.16f)},
            {"backLightPosition",  new Vector3(-0.322f, 1.502f, -0.185f)},

            {"keyLightRotation", new Vector3(45f, -135f, 26.216f)},
            {"fillLightRotation", new Vector3(0f, 0f, 0f)},
            {"backLightRotation", new Vector3(0f, 0f, 0f)}
        };

        public Dictionary<string, Color> lightRigColours = new Dictionary<string, Color>()
        {
            {"keyLight", new Color(57f, 110f, 221f, 255f)},
            {"fillLight", Color.white},
            {"backLight", new Color(230f, 33f, 80f, 255f)}
        };

        public Dictionary<string, float> lightsAdjustments = new Dictionary<string, float>()
        {
            {"keyLightIntensity", 0.025f},
            {"fillLightIntensity", 0.5f},
            {"backLightIntensity", 0.013f},

            {"keyLightRange", 3.85f},
            {"fillLightRange", 0.25f},
            {"backLightRange", 1.08f},

            {"keyLightSpotAngle", 18.52f},
            {"fillLightSpotAngle", 0f},
            {"backLightSpotAngle", 0f }

        };
    }
}
