﻿
using System.Collections.Generic;
using UnityEngine;

namespace Assets.SceneEditor.Default_Light_Rigs
{
    public class SurprisedLightRig
    {
        public Dictionary<string, Vector3> lightRigTranslations = new Dictionary<string, Vector3>()
        {
            {"keyLightPosition",  new Vector3(0.5853867f, 2.442315f, 0.621f)},
            {"fillLightPosition",  new Vector3(0.049f, 1.563f, 0.326f)},
            {"backLightPosition",  new Vector3(-0.46f, 1.794906f, -0.189f)},

            {"keyLightRotation", new Vector3(135f, 45f, 29f)},
            {"fillLightRotation", new Vector3(0f, 0f, 0)},
            {"backLightRotation", new Vector3(0f, 0f, 0)}
        };

        public Dictionary<string, Color> lightRigColours = new Dictionary<string, Color>()
        {
            {"keyLight", Color.white},
            {"fillLight", Color.white},
            {"backLight", new Color(192f, 37f, 221f, 255f)}
        };

        public Dictionary<string, float> lightsAdjustments = new Dictionary<string, float>()
        {
            {"keyLightIntensity", 3f},
            {"fillLightIntensity", 0.9f},
            {"backLightIntensity", 0.01f},

            {"keyLightRange", 2.3f},
            {"fillLightRange", 0.62f},
            {"backLightRange", 1.14f},

            {"keyLightSpotAngle", 12.21f},
            {"fillLightSpotAngle", 0f},
            {"backLightSpotAngle", 0f }

        };
    }
}
