﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.SceneEditor.Default_Light_Rigs
{
    public class HappyLightRig
    {
        public Dictionary<string, Vector3> lightRigTranslations = new Dictionary<string, Vector3>()
            {
                {"keyLightPosition",  new Vector3(0.478f, 1.885f, 0.792f)},
                {"fillLightPosition",  new Vector3(-0.184f, 1.661f, 0.277f)},
                {"backLightPosition",  new Vector3(-0.212f, 1.491245f, -0.343f)}
            };

        public Dictionary<string, Color> lightRigColours = new Dictionary<string, Color>()
            {
                {"keyLight", new Color(231f, 132f, 80f, 255f)},
                {"fillLight", Color.white},
                {"backLight", Color.white}
            };

        public Dictionary<string, float> lightsAdjustments = new Dictionary<string, float>()
            {
                {"keyLightIntensity", 0.04f},
                {"fillLightIntensity", 2f},
                {"backLightIntensity", 3.02f},

                {"keyLightRange", 1.152508f},
                {"fillLightRange", 0.4810469f},
                {"backLightRange", 0.8518539f},
            };
    }
}
