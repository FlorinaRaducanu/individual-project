Controlling Visual Effects in 3D Virtual Environment Based on the Facial Expressions of the Avatar 

Documentation on how to open the Scene Editor

1. Clone the master repository
2. Once the repositior is cloned, go to the individual-project/SceneEditor/Assets 
3. Open the SceneEditorTest.unity file 
4. Once unity is open, click on Tools>EditorScene in the top menu bar 

Instruction on how to generate a lighting rig 

1. Choose a game object from the Hierarchy Window
2. and a setting corresponding to an emotion from the dropdown menu 
3. Click on the 'New Lighting Rig' once it shows up shows up

Instructions on how to use the camer editor
1. click in the camera menu button 
2. change the position and rotation of the camera
Optional: Point the camera at any selected object in the scene by clicking on the prefered object to activate it and then tick the 'Look at selected object' checkbox
